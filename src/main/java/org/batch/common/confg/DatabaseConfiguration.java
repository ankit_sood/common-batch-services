package org.batch.common.confg;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class DatabaseConfiguration {

	private final Logger log = LoggerFactory.getLogger(DatabaseConfiguration.class);

	private final Environment env;

	public DatabaseConfiguration(Environment env) {
		this.env = env;
	}

	@Bean(name = "batchH2DataSource")
	@Qualifier(value = "batchH2DataSource")
	@Primary
	public DataSource batchH2DataSource() {
		log.info("Intializing bean of h2DataSource");
		return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2).ignoreFailedDrops(true)
				.addScript("schema.sql").continueOnError(true).build();
	}

	@Bean(name = "batchTransactionManager")
	public PlatformTransactionManager transactionManager() {
		return new ResourcelessTransactionManager();
	}
}

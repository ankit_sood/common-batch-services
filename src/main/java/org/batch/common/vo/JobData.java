package org.batch.common.vo;

public class JobData {
	private String fileType;
	private String inputFileName;
	private String filePath;
	private String jobBeanConfigName;
	private boolean quoteCharacter;
	private String delimiter;
	private String mandatoryCols;
	
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getInputFileName() {
		return inputFileName;
	}
	public void setInputFileName(String inputFileName) {
		this.inputFileName = inputFileName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getJobBeanConfigName() {
		return jobBeanConfigName;
	}
	public void setJobBeanConfigName(String jobBeanConfigName) {
		this.jobBeanConfigName = jobBeanConfigName;
	}
	public boolean isQuoteCharacter() {
		return quoteCharacter;
	}
	public void setQuoteCharacter(boolean quoteCharacter) {
		this.quoteCharacter = quoteCharacter;
	}
	public String getDelimiter() {
		return delimiter;
	}
	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}
	public String getMandatoryCols() {
		return mandatoryCols;
	}
	public void setMandatoryCols(String mandatoryCols) {
		this.mandatoryCols = mandatoryCols;
	}
}

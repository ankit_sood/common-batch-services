package org.batch.common.vo;

public class BaseVO {
	private boolean isValid = true;

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}
}

package org.batch.common.job.processor;

import java.util.List;

import org.batch.common.validators.IValidator;
import org.batch.common.vo.BaseVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

public class BasicCsvItemProcessor<T extends BaseVO,O extends BaseVO> implements ItemProcessor<T, O>{
	private final Logger log = LoggerFactory.getLogger(BasicCsvItemProcessor.class);
	private List<IValidator<T>> validators;
	
	public BasicCsvItemProcessor(List<IValidator<T>> validators) {
		super();
		this.validators = validators;
	}

	@Override
	public O process(T item) throws Exception {
		log.info("processing item: "+item);
		boolean isValid = checkRecordValidity(item);
		item.setValid(isValid);
		return (O) item;
	}
	
	private boolean checkRecordValidity(T item) {
		boolean isValid = true;
		for(IValidator<T> validator: validators) {
			isValid = validator.validate(item);
			if(!isValid) {
				break;
			}
		}
		return isValid;
	}

}

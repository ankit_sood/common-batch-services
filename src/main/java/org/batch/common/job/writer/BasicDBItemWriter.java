package org.batch.common.job.writer;

import java.util.List;

import org.springframework.batch.item.ItemWriter;

public class BasicDBItemWriter<O> implements ItemWriter<O>{

	@Override
	public void write(List<? extends O> items) throws Exception {
		for(O item:items) {
			System.err.println("BasicDBWritter: "+item);
		}
	}

}

package org.batch.common.job.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.batch.common.constants.HeadersConstants;
import org.batch.common.job.processor.BasicCsvItemProcessor;
import org.batch.common.job.reader.BasicCsvItemReader;
import org.batch.common.job.writer.BasicDBItemWriter;
import org.batch.common.validators.IValidator;
import org.batch.common.vo.BaseVO;
import org.batch.common.vo.JobData;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.builder.SimpleStepBuilder;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.core.io.FileSystemResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.util.ResourceUtils;

public class BasicCsvToDBJobItemConfigurer<T extends BaseVO,O extends BaseVO> implements BatchJobConfigurer<T,O>{

	private final StepBuilderFactory stepBuilderFactory;
	private final Class<T> clazz;
	private final JobData jobData;
	
	public BasicCsvToDBJobItemConfigurer(StepBuilderFactory stepBuilderFactory, JobData jobData, Class<T> clazz) {
		super();
		this.stepBuilderFactory = stepBuilderFactory;
		this.clazz = clazz;
		this.jobData = jobData;
	}

	@Override
	public ItemReader<T> getItemReaderBean() {
		FileSystemResourceLoader fileSystemResourceLoader = new FileSystemResourceLoader();
		Resource resource = fileSystemResourceLoader.getResource(ResourceUtils.FILE_URL_PREFIX+jobData.getFilePath()+jobData.getInputFileName());
		FlatFileItemReader<T> flatFileReader = new FlatFileItemReaderBuilder<T>()
																			.name("basicCsvFileReader")
																			.resource(resource)
																			.linesToSkip(1)
																			.strict(false)
																			.build();
		return new BasicCsvItemReader<>(flatFileReader);
	}

	@Override
	public ItemProcessor<T, O> getItemProcessorBean() {
		return new BasicCsvItemProcessor<T,O>(getValidations());
	}

	@Override
	public ItemWriter<O> getItemWriterBean() {
		return new BasicDBItemWriter<O>();
	}

	@Override
	public SimpleStepBuilder<T, O> getItemFileStepBuilder() {
		return stepBuilderFactory.get("basicCsvFileToDBStep").<T,O>chunk(10);
	}

	@Override
	public LineMapper<T> getLineMapper() {
		return getDefaultLineMapper(jobData.getFilePath()+jobData.getInputFileName());
	}
	
	@Override
	public List<IValidator<T>> getValidations() {
		List<IValidator<T>> validators = new ArrayList<>();
		return validators;
	}
	
	private Class<T> getGenericClass(){
		return clazz;
	}
	
	private LineMapper<T> getDefaultLineMapper(String filePath){
		DefaultLineMapper<T> defaultLineMapper = new DefaultLineMapper<>();
		defaultLineMapper.setLineTokenizer(getInputFileLineTokenizer(filePath));
		defaultLineMapper.setFieldSetMapper(inputInformationMapper());
		return defaultLineMapper;
	}
	
	private LineTokenizer getInputFileLineTokenizer(String filePath) {
		DelimitedLineTokenizer delimitedLineTokenizer = new DelimitedLineTokenizer(jobData.getDelimiter());
		delimitedLineTokenizer.setStrict(false);
		if(jobData.isQuoteCharacter()) {
			delimitedLineTokenizer.setQuoteCharacter('"');
		}
		delimitedLineTokenizer.setNames(getHeaders(filePath));
		return delimitedLineTokenizer;
	}
	
	private FieldSetMapper<T> inputInformationMapper(){
		BeanWrapperFieldSetMapper<T> inputInfMapper = new BeanWrapperFieldSetMapper<>();
		inputInfMapper.setTargetType(getGenericClass());
		return inputInfMapper;
	}
	
	private String[] getHeaders(String filePath) {
		File file = new File(filePath);
		String[] resultantHeaders = new String[8];
		try(BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line = br.readLine();
			String[] headers = line.split(",");
			resultantHeaders = new String[headers.length];
			for(int i=0;i<headers.length;i++) {
				resultantHeaders[i] = HeadersConstants.headersMap.get(headers[i]);
			}
		} catch(IOException exp) {
			System.out.println("Exception occured while getting the headers from the file: "+exp.getMessage());
		}
		return resultantHeaders;
	}

}

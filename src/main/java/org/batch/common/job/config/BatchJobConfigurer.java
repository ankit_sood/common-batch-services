package org.batch.common.job.config;

import java.util.List;

import org.batch.common.validators.IValidator;
import org.springframework.batch.core.step.builder.SimpleStepBuilder;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.LineMapper;

public interface BatchJobConfigurer<T,O> {
	ItemReader<T> getItemReaderBean();
	
	ItemProcessor<T,O> getItemProcessorBean();
	
	ItemWriter<O> getItemWriterBean();
	
	SimpleStepBuilder<T, O> getItemFileStepBuilder();
	
	LineMapper<T> getLineMapper();
	
	List<IValidator<T>> getValidations();
}

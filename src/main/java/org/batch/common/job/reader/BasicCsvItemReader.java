package org.batch.common.job.reader;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.file.FlatFileItemReader;

public class BasicCsvItemReader<T> implements ItemReader<T>{
	private final FlatFileItemReader<T> flatFileReader;
	private ExecutionContext stepExecutionContext;
	private ExecutionContext jobExecutionContext;
	
	public BasicCsvItemReader(FlatFileItemReader<T> flatFileReader) {
		super();
		this.flatFileReader = flatFileReader;
	}

	@Override
	public T read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		flatFileReader.open(stepExecutionContext);
		T item = flatFileReader.read();
		return item;
	}

	public void setStepExecutionContext(ExecutionContext stepExecutionContext) {
		this.stepExecutionContext = stepExecutionContext;
	}

	public void setJobExecutionContext(ExecutionContext jobExecutionContext) {
		this.jobExecutionContext = jobExecutionContext;
	}

}

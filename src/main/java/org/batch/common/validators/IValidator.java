package org.batch.common.validators;

public interface IValidator<T> {
	public boolean validate(T item);
}
